from datetime import datetime
from json import load
from os.path import join
from typing import Dict, Tuple, List, Any, Union
import pandas as pd
import os
from utils.utils import heroes_names

default_keys = ('match_id', 'radiant_win', 'duration', 'start_time', 'region', 'picks_bans',
                'radiant_gold_adv', 'radiant_xp_adv', 'dire_score', 'radiant_score')


def get_teams(pick_ban: List[Dict[str, Union[bool, int]]]) -> Dict[str, int]:
    """
    Extract the picked heroes for each team
    """

    # Extract the picked heroes for each team
    radiant = [row['hero_id'] for row in pick_ban if row['is_pick'] and row['team'] == 0]
    dire = [row['hero_id'] for row in pick_ban if row['is_pick'] and row['team'] == 1]

    # Extract the banned heroes for each team
    # radiant_banned = [row['hero_id'] for row in pick_ban if not row['is_pick'] and row['team'] == 0]
    # dire_banned = [row['hero_id'] for row in pick_ban if not row['is_pick'] and row['team'] == 1]

    heroes = {hero: [0] for hero in heroes_names.values()}
    for radiant_hero, dire_hero in zip(radiant[-5:], dire[-5:]):
        heroes[heroes_names[radiant_hero]][0] += 1
        heroes[heroes_names[dire_hero]][0] -= 1

    return heroes


def get_match_data(match: Dict[str, Any], keys: Tuple[str]) -> Dict[str, str]:
    """
    Extract the desired data from the json
    """
    match_data = {key: match[key] for key in keys}
    if 'picks_bans' in keys:

        heroes = get_teams(match_data['picks_bans'])
        match_data.pop('picks_bans')
        match_data.update(heroes)

    if 'radiant_gold_adv' in keys:

        gold_adv = match_data['radiant_gold_adv'][-1]
        match_data['radiant_gold_adv'] = gold_adv

    if 'radiant_xp_adv' in keys:

        xp_adv = match_data['radiant_xp_adv'][-1]
        match_data['radiant_xp_adv'] = xp_adv

    if 'start_time' in keys:

        timestamp = match_data['start_time']
        match_data['start_time'] = datetime.fromtimestamp(timestamp)

    return match_data


def read_file(folderPath: str, fileName: str) -> Dict:
    """
    Gets the json file into a dictionary

    :param folderPath: path to the json file
    :param fileName: name of the jason file
    :return: dictionary with the match infos
    """
    with open(join(folderPath, fileName), "r") as outfile:
        file = load(outfile)

    return file


def save_dataframe(folder_path: str, output_path: str = "", keys: Tuple = default_keys) -> None:
    """
    Transforms the files in MatchData a dataframe and saves it into a csv
    Default output_path is the script folder

    :param folder_path: path where the JSON matches are
    :param output_path: where the csv will be saved
    :param keys: name of the wanted attributes
    """
    df = pd.DataFrame(columns=keys + tuple(heroes_names.values()))
    count = 0
    for file in os.listdir(folder_path):
        if file.endswith(".json"):
            match = read_file(folder_path, file)
            count += 1
            if is_match_valid(match):
                match_data = get_match_data(match, keys)
                match_df = pd.DataFrame(match_data)
                df = pd.concat([df, match_df], axis=0)

    # Let's reset the index
    df = df.reset_index(drop=True)
    # Some data has null value for picks_bans, so we get an extra column with NaN values
    df = df.drop(['picks_bans'], axis=1)

    print(f'{df.shape[0]} out of {count} ({df.shape[0]/count:.2%}) are Valid Files')

    df.to_csv(f'{output_path}Dota2MatchesData.csv')
    print(f'Saved CSV data into:\n{join(output_path,"Dota2MatchesData.csv")}')

    print('Finished')


def is_match_valid(match_dict: Dict[str, Any], keys: Tuple[str] = default_keys) -> bool:
    """
    Checks if all the players finished the match, the game_mode is All Pick, Captain's Mode, Random Draft,
     Single Draft, All Random or Ranked All Pick (1, 2, 3, 4, 5, 22), there are all keys in the dictionary
     and the teams have at least 5 players

    :param match_dict: dictionary containing match information
    :param keys: keys that the match_dict must have
    :return: true if valid
    """
    players_finished = all([player['leaver_status'] == 0 for player in match_dict['players']])
    valid_game_mode = match_dict['game_mode'] in [1, 2, 3, 4, 5, 22]
    has_all_keys = all([key in match_dict.keys() and match_dict[key] is not None for key in keys])
    if has_all_keys:
        team_dire = sum(map(lambda x: x['team'], match_dict['picks_bans']))
        team_radiante = len(match_dict['picks_bans']) - team_dire
    else:
        team_dire, team_radiante = False, False
    return players_finished and valid_game_mode and has_all_keys and team_dire >= 5 and team_radiante >= 5


if __name__ == '__main__':

    ParsedPath = "ParsedMatches"
    matches_path = "MatchData"

    save_dataframe(matches_path)
