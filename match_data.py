from functools import wraps
from requests import get
from json import dump
from time import sleep, time, strftime
from os.path import join, exists
from os import makedirs
from typing import Callable
from threading import Timer


def timed_function(func: Callable) -> Callable:
    """
    Decorator to count the time that a function takes to run
    """
    @wraps(func)
    def wrap(*args, **kwargs):
        ts = time()
        result = func(*args, **kwargs)
        te = time()
        print(f'func:{func.__name__} args:[{args}, {kwargs}] took: {te - ts} sec')
        return result
    return wrap


def get_match(match_id):
    """
    This function will get the match data from the string
    and return a json file
    """
    url = "https://api.opendota.com/api/matches/"
    data = get(join(url, match_id))

    return data.json()


def save_match(match, output_path):
    """
    Will save the json file into the directory
    """
    global matches_number
    if "match_id" not in match.keys():
        print(match)
        print(type(match))
        return
    match_id = match["match_id"]

    with open(join(output_path, f"Match_{match_id}.json"), "w") as outfile:
        matches_number += 1
        dump(match, outfile)
        print(f"{matches_number:05} - Match {match_id} Saved")


def get_list_parsed_matches(parsed_path: str):
    """
    Will get random samples of parsed matches,
    parsed matches has all data fields filled.

    ParsedPath -> Output FOLDER Path
    """
    # Start by requesting the data
    url = "https://api.opendota.com/api/parsedMatches"
    data = get(url)
    parsed_matches = data.json()

    # Let's organize the data based on time of request

    with open(join(parsed_path, f"ParsedList_{strftime('%m.%d.%Y_%H.%M.%S')}.json"), "w") as outfile:
        dump(parsed_matches, outfile)
        print("Parsed List Saved")

    return parsed_matches


def get_parsed_list(json_file, output_path):
    """
    Gets the match data from a list of match id from json file

    InputPath -> Input FILE Path
    OutputPath -> Output FOLDER Path
    """
    # Every item in the list is dictionary, we only need the match id
    matches = map(lambda v: v['match_id'], json_file)

    start_time = time()
    for i, match_id in enumerate(matches):
        sleep(2)
        match = get_match(str(match_id))
        save_match(match, output_path)
        # Sleep times to not exceed requests number
        if i == 30:
            sleep(max((50 - (time() - start_time)), 0))
        if i == 65:
            sleep(max((100 - (time() - start_time)), 0))


@timed_function
def get_cycle(parsed_path, output_path):
    """
    Runs a single cycle of data collecting,
    Parsed Matches json -> 100 Individual matches json

    ParsedPath -> Input FOLDER Path
    OutputPath -> Output FOLDER Path
    """
    # Start by getting 100 match id samples
    json_file = get_list_parsed_matches(parsed_path)

    get_parsed_list(json_file, output_path)


class RepeatTimer(Timer):
    """
    Class to run a function every 'self.interval'
    """
    def run(self) -> None:
        global matches_number
        self.function(*self.args, **self.kwargs)
        while not self.finished.wait(self.interval):
            print(matches_number)
            self.function(*self.args, **self.kwargs)


if __name__ == '__main__':
    matches_number = 0
    parsed_path = "ParsedMatches"
    output_path = "MatchData"

    if not exists(parsed_path):
        makedirs(parsed_path)
    if not exists(output_path):
        makedirs(output_path)

    timer = RepeatTimer(150, get_cycle, [parsed_path, output_path])
    timer.start()
    while matches_number < 45000:
        sleep(150)
    timer.cancel()

