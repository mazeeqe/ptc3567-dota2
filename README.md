# Predicting the winner team of DOTA 2 matches
___

This is a Data Science Project for a college class (<a href="https://uspdigital.usp.br/jupiterweb/obterDisciplina?nomdis=&sgldis=PTC3567">PTC3567 - Ciência dos Dados em Automação e Engenharia</a>).

## Description
This project aims to predict the winner of a Dota 2 match based on features selected by the group. We've used 3 different algorthm models: K-Nearest Neighbor Classifier, Random Forest Classifier and Support Vector Classifier


## Setting Up
This project uses <a href="https://www.python.org/">Python 3</a>.

If you have already installed it in your machine, you can create a virtual environment and install the used packages:
```shell
python -m venv venv
venv/scripts/activate
pip install -r requirements.txt
```
## Usage
### Getting Matches Data
```shell
python match_data.py
```
It will create 'ParsedMatches' and 'MatchData' folders, and start to download the matches data every 2.5 minutes. 

### Pre-Processing the Data
```shell
python extract_data.py
```
Creates the 'Dota2MatchesData.csv' from the data in 'MatchData'

### K-Nearest Neighbor Classifier
```shell
python .\KNearestNeighborsClassifier\train_classifier.py
```
<div style="margin: 20px 5%;">
options:
  <div style="margin: 10px 5%;">
  <p>-h, --help</p>
                        <p style="text-indent: 30px">show this help message and exit</p>
  <p>--csv_data_path [default='.\']</p>
                        <p style="text-indent: 30px">path to the csv file</p>
  <p>--csv_data_name [default='Dota2MatchesData.csv']</p>
                        <p style="text-indent: 30px">name of the csv file</p>
  <p>--test_size [default=0.3]</p>
                        <p style="text-indent: 30px">proportion of the dataset to include in the test split</p>
  <p>--random_state [default=None]</p>
                        <p style="text-indent: 30px">controls the shuffling applied to the data before
                        applying the split between test and train</p>
  <p>--heroes [default=True]</p>
                        <p style="text-indent: 30px">true to include heroes in the dataset</p>
  <p>--n_neighbors [default=5]</p>
                        <p style="text-indent: 30px">number of neighbors to use for k-neighbors queries</p>
  <p>--weights {uniform,distance} [default='distance']</p>
                        <p style="text-indent: 30px">weight function used in prediction</p>
  <p>--save_model_name [default=None]</p>
                        <p style="text-indent: 30px">the archive name that saves the model if you wants it
                        to be saved</p>
  <p>--metrics [default=('confusion matrix', 'accuracy')]</p>
                        <p style="text-indent: 30px">name of the metrics wanted</p>
  <p>--drop_columns [{duration, start_time, region, radiant_gold_adv, radiant_xp_adv, dire_score, radiant_score}] [default=('duration', 'start_time', 'region')]</p>
                        <p style="text-indent: 30px">name of the unwanted columns</p>
  </div>
</div>

### Random Forest Classifier
```shell
python .\RandomForestClassifier\train_classifier.py
```
<div style="margin: 20px 5%;">
options:
  <div style="margin: 10px 5%;">
  <p>-h, --help</p>
                        <p style="text-indent: 30px">show this help message and exit</p>
  <p>--csv_data_path [default='.\']</p>
                        <p style="text-indent: 30px">path to the csv file</p>
  <p>--csv_data_name [default='Dota2MatchesData.csv']</p>
                        <p style="text-indent: 30px">name of the csv file</p>
  <p>--test_size [default=0.3]</p>
                        <p style="text-indent: 30px">proportion of the dataset to include in the test split</p>
  <p>--random_state [default=None]</p>
                        <p style="text-indent: 30px">controls the shuffling applied to the data before
                        applying the split between test and train</p>
  <p>--heroes [default=True]</p>
                        <p style="text-indent: 30px">true to include heroes in the dataset</p>
  <p>--number_of_trees [default=100]</p>
                        <p style="text-indent: 30px">the number of trees in the forest</p>
  <p>--max_tree_depth [default=None]</p>
                        <p style="text-indent: 30px">the maximum depth of the tree</p>
  <p>--save_model_name [default=None]</p>
                        <p style="text-indent: 30px">the archive name that saves the model if you wants it
                        to be saved</p>
  <p>--metrics [default=('confusion matrix', 'accuracy')]</p>
                        <p style="text-indent: 30px">name of the metrics wanted</p>
  <p>--drop_columns [{duration, start_time, region, radiant_gold_adv, radiant_xp_adv, dire_score, radiant_score}] [default=('duration', 'start_time', 'region')]</p>
                        <p style="text-indent: 30px">name of the unwanted columns</p>
  </div>
</div>

### Support Vector Machine Classifier
```shell
python .\SuportVectorClassifier\train_classifier.py
```
<div style="margin: 20px 5%;">
options:
  <div style="margin: 10px 5%;">
  <p>-h, --help</p>
                        <p style="text-indent: 30px">show this help message and exit</p>
  <p>--csv_data_path [default='.\']</p>
                        <p style="text-indent: 30px">path to the csv file</p>
  <p>--csv_data_name [default='Dota2MatchesData.csv']</p>
                        <p style="text-indent: 30px">name of the csv file</p>
  <p>--test_size [default=0.3]</p>
                        <p style="text-indent: 30px">proportion of the dataset to include in the test split</p>
  <p>--random_state [default=None]</p>
                        <p style="text-indent: 30px">controls the shuffling applied to the data before applying the split</p>
  <p>--heroes [default=True]</p>
                        <p style="text-indent: 30px">true to include heroes in the dataset</p>
  <p>--kernel {'linear', 'poly', 'rbf', 'sigmoid'} [default='rbf]</p>
                        <p style="text-indent: 30px">specifies the kernel type to be used in the algorithm</p>
  <p>--save_model_name [default=None]</p>
                        <p style="text-indent: 30px">the archive name that saves the model if you wants it to be saved</p>
  <p>--metrics [default=('confusion matrix', 'accuracy')]</p>
                        <p style="text-indent: 30px">name of the metrics wanted</p>
  <p>--drop_columns [{duration, start_time, region, radiant_gold_adv, radiant_xp_adv, dire_score, radiant_score}] [default=('duration', 'start_time', 'region')]</p>
                        <p style="text-indent: 30px">name of the unwanted columns</p>
  </div>
</div>

#### Metrics
Possible metrics:
<div style="margin: 20px 5%;">
    <ul style="text-indent:0; margin-left: 3%; text-align: justify">
        <li>accuracy</li>
        <li>prevalence</li>
        <li>balanced accuracy</li>
        <li>f1 score</li>
        <li>phi coefficient</li>
        <li>matthews correlation coefficient</li>
        <li>fowlkes–mallows index</li>
        <li>informedness</li>
        <li>bookmaker informedness</li>
        <li>markedness</li>
        <li>deltaP</li>
        <li>diagnostic odds ratio</li>
        <li>sensitivity</li>
        <li>recall</li>
        <li>hit rate</li>
        <li>true positive rate</li>
        <li>specificity</li>
        <li>selectivity</li>
        <li>true negative rate</li>
        <li>precision</li>
        <li>positive predictive value</li>
        <li>negative predictive value</li>
        <li>miss rate</li>
        <li>false negative rate</li>
        <li>fall-out</li>
        <li>false positive rate</li>
        <li>false discovery rate</li>
        <li>false omission rate</li>
        <li>positive likelihood ratio</li>
        <li>negative likelihood ratio</li>
        <li>prevalence threshold</li>
        <li>threat score</li>
        <li>critical success index</li>
        <li>confusion matrix</li>
    </ul>
</div>

