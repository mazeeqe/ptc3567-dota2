from os.path import join, dirname, realpath
from sys import path
from pickle import load

import pandas as pd
import numpy as np
from math import sqrt
from requests import get
from typing import Dict, Optional, Iterable, Union, Tuple

from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# region to get and append the parent directory path
current_path = dirname(realpath(__file__))
parent_path = dirname(current_path)
path.append(parent_path)
# endregion


def get_heroes_name(url: str = "https://api.opendota.com/api/heroes") -> Dict[int, str]:
    """
    Get hero name and id from url
    :param url: address for the heroes json
    :return: dictionary with hero id as keys and hero name as values
    """
    data = get(url)
    df = pd.DataFrame(data=data.json())
    return {row['id']: row['localized_name'] for _, row in df.iterrows()}


heroes_names = get_heroes_name()


def binary_classifier_metrics(tn: int, fp: int, fn: int, tp: int) -> Union[Dict[str, Tuple[str, float]],
                                                                           Dict[str, Tuple[str, Tuple[int]]]]:
    """
    Implementation of all binary metrics given at https://en.wikipedia.org/wiki/Evaluation_of_binary_classifiers
    :param tn: true negative
    :param fp: false positive
    :param fn: false negative
    :param tp: true positive
    :return: dictionary with the name of the metric as key and a tuple with a string and a float with its value as value
    """
    p = tp + fn
    n = fp + tn
    tpr = tp / p
    tnr = tn / n
    ppv = tp / (tp + fp)
    npv = tn / (tn + fn)
    fpr = 1 - tnr
    fnr = 1 - tpr
    mcc = (tp * tn - fp * fn) / sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))
    lr_plus = tpr / fpr
    lr_minus = fnr / tnr
    metrics = dict()
    metrics['accuracy'] = (f'\tAccuracy: {(tp + tn) / (tp + tn + fp + fn):.3%}', (tp + tn) / (tp + tn + fp + fn))
    metrics['prevalence'] = (f'\tPrevalence: {p / (p + n):.3%}', p / (p + n))
    metrics['balanced accuracy'] = (f'\tBalanced Accuracy: {(tpr + tnr) / 2:.3%}', (tpr + tnr) / 2)
    metrics['f1 score'] = (f'\tF1 Score: {(2 * tp) / (2 * tp + fp + fn):.3%}', (2 * tp) / (2 * tp + fp + fn))
    metrics['phi coefficient'] = (f'\t\u03C6: {mcc:.3%}', mcc)
    metrics['matthews correlation coefficient'] = (f'Matthews Correlation Coefficient: {mcc:.3%}', mcc)
    metrics['fowlkes–mallows index'] = (f'\tFowlkes–Mallows Index: {sqrt(ppv*tpr):.3%}', sqrt(ppv*tpr))
    metrics['informedness'] = (f'\tInformedness: {tpr + tnr - 1:.3%}', tpr + tnr - 1)
    metrics['bookmaker informedness'] = (f'\tBookmaker Informedness: {tpr + tnr - 1:.3%}', tpr + tnr - 1)
    metrics['markedness'] = (f'\tMarkedness: {ppv + npv - 1:.3%}', ppv + npv - 1)
    metrics['deltaP'] = (f'\t\u0394P: {ppv + npv - 1:.3%}', ppv + npv - 1)
    metrics['diagnostic odds ratio'] = (f'\tDiagnostic Odds Ratio: {lr_plus / lr_minus:.3%}', lr_plus / lr_minus)
    metrics['sensitivity'] = (f'\tSensitivity: {tpr:.3%}', tpr)
    metrics['recall'] = (f'\tRecall: {tpr:.3%}', tpr)
    metrics['hit rate'] = (f'\tHit Rate: {tpr:.3%}', tpr)
    metrics['true positive rate'] = (f'\tTrue Positive Rate: {tpr:.3%}', tpr)
    metrics['specificity'] = (f'\tSpecificity: {tnr:.3%}', tnr)
    metrics['selectivity'] = (f'\tSelectivity: {tnr:.3%}', tnr)
    metrics['true negative rate'] = (f'\tTrue Negative Rate: {tnr:.3%}', tnr)
    metrics['precision'] = (f'\tPrecision: {ppv:.3%}', ppv)
    metrics['positive predictive value'] = (f'\tPositive Predictive Value: {ppv:.3%}', ppv)
    metrics['negative predictive value'] = (f'\tNegative Predictive Value: {npv:.3%}', npv)
    metrics['miss rate'] = (f'\tMiss Rate: {fnr:.3%}', fnr)
    metrics['false negative rate'] = (f'\tFalse Negative Rate: {fnr:.3%}', fnr)
    metrics['fall-out'] = (f'\tFall-Out: {fpr:.3%}', fpr)
    metrics['false positive rate'] = (f'\tFalse Positive Rate: {fpr:.3%}', fpr)
    metrics['false discovery rate'] = (f'\tFalse Discovery Rate: {1 - ppv:.3%}', 1 - ppv)
    metrics['false omission rate'] = (f'\tFalse Omission Rate: {1 - npv:.3%}', 1 - npv)
    metrics['positive likelihood ratio'] = (f'\tPositive Likelihood Ratio: {lr_plus:.3%}', lr_plus)
    metrics['negative likelihood ratio'] = (f'\tNegative Likelihood Ratio: {lr_minus:.3%}', lr_minus)
    metrics['prevalence threshold '] = (f'\tPrevalence Threshold : {sqrt(fpr) / (sqrt(tpr) + sqrt(fpr)):.3%}',
                                        sqrt(fpr) / (sqrt(tpr) + sqrt(fpr)))
    metrics['threat score'] = (f'\tThreat Score: {tp / (tp + fn + fp):.3%}', tp / (tp + fn + fp))
    metrics['critical success index'] = (f'\tCritical Success Index: {tp / (tp + fn + fp):.3%}', tp / (tp + fn + fp))
    metrics['confusion matrix'] = (f'''
                              ┌────────────────────┬────────────────────┐
                              │ Predicted Radiant  │   Predicted Dire   │
            ┌─────────────────┼────────────────────┼────────────────────┤
            │   Radiant Won   │ {tp:^18_} │ {fn:^18_} │
            ├─────────────────┼────────────────────┼────────────────────┤
            │     Dire Won    │ {fp:^18_} │ {tn:^18_} │
            └─────────────────┴────────────────────┴────────────────────┘
    '''.replace('_', ' '), (tn, fp, fn, tp))

    return metrics


binary_classifier_metrics_names = ['accuracy', 'prevalence', 'balanced accuracy', 'f1 score', 'phi coefficient',
                                   'matthews correlation coefficient', 'fowlkes–mallows index', 'informedness',
                                   'bookmaker informedness', 'markedness', 'deltaP', 'diagnostic odds ratio',
                                   'sensitivity', 'recall', 'hit rate', 'true positive rate', 'specificity',
                                   'selectivity', 'true negative rate', 'precision', 'positive predictive value',
                                   'negative predictive value', 'miss rate', 'false negative rate', 'fall-out',
                                   'false positive rate', 'false discovery rate', 'false omission rate',
                                   'positive likelihood ratio', 'negative likelihood ratio', 'prevalence threshold ',
                                   'threat score', 'critical success index', 'confusion matrix']


def create_match(heroes: Optional[Dict[str, Iterable[str]]] = None) -> np.ndarray:
    """
    Creates an input array with the name of the heroes

    :param heroes: dictionary containing the name of the team as keys and the hero names as values
    :return: array in the input shape for the classifiers
    """
    names = [*heroes_names.values()]
    if heroes is None:
        radiant, dire = [], []

        print('Criando uma partida')
        for i in range(5):
            while True:
                name = input(f'\t{i + 1}º herói do time Radiant: ').title()
                if name in names and name not in radiant:
                    radiant.append(name)
                    break
                else:
                    print(f'\t\tEsse herói ({name}) não existe ou já foi escolhido')

        print()
        for i in range(5):
            while True:
                name = input(f'\t{i + 1}º herói do time Dire: ').title()
                if name in names and name not in dire and name not in radiant:
                    dire.append(name)
                    break
                else:
                    print(f'\t\tEsse herói ({name}) não existe ou já foi escolhido')

        heroes = {'Radiant': radiant, 'Dire': dire}

    df = pd.DataFrame({hero: [0] for hero in names})
    for radiant_hero in heroes['Radiant']:
        df[radiant_hero].iloc[0] += 1
    for dire_hero in heroes['Dire']:
        df[dire_hero].iloc[0] -= 1

    return df.values


def load_classifier(save_name: str) -> Union[RandomForestClassifier, KNeighborsClassifier, SVC]:
    """
    Load the saved classifier

    :param save_name: classifier archive name
    :return: classifier object
    """
    with open(join(current_path, save_name), 'rb') as file:
        clf = load(file)

    return clf
