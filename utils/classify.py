from os.path import dirname, realpath
from sys import path
from argparse import ArgumentParser
from typing import Iterable, Optional, Union
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
import numpy as np

# region to get and append the parent directory path
current_path = dirname(realpath(__file__))
parent_path = dirname(current_path)
path.append(parent_path)
# endregion

from utils.utils import create_match, heroes_names, load_classifier


def prediction(clf: Union[RandomForestClassifier, KNeighborsClassifier, SVC], match: np.ndarray) -> None:
    """
    Predict the winning team
    :param clf: classifier
    :param match: input data
    """
    print({0: 'Dire Wins', 1: 'Radiant Wins'}[clf.predict(match)[0]])


def main(model_arq_name: str,
         radiant: Optional[Iterable[str]] = None,
         dire: Optional[Iterable[str]] = None) -> None:
    """
    Main function that calls the other functions in order with the right parameters

    :param model_arq_name: classifier archive name
    :param radiant: contains the radiant heroes names
    :param dire: contains the dire heroes names
    """
    if radiant is not None and dire is not None and len(set.union(set(radiant), set(dire))) == 10:
        match = create_match({'Radiant': radiant, 'Dire': dire})
    else:
        match = create_match()
    clf = load_classifier(model_arq_name)
    prediction(clf, match)


if __name__ == '__main__':
    parser = ArgumentParser(prog='')
    parser.add_argument('model_arq_name', type=str)
    parser.add_argument('--radiant', type=str, nargs=5, choices=[*heroes_names.values()])
    parser.add_argument('--dire', type=str, nargs=5, choices=[*heroes_names.values()])

    args = parser.parse_args()

    main(**vars(args))
