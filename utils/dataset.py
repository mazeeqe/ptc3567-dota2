import pandas as pd
from sys import path
from os.path import dirname, realpath, join
from typing import Iterable

# region to get and append the parent_path directory path
current = dirname(realpath(__file__))
parent = dirname(current)
path.append(parent)
# endregion


def load_csv_dataset(file_path: str = parent,
                     file_name: str = 'Dota2MatchesData.csv',
                     drop_cols: Iterable[str] = ('duration', 'start_time', 'region'),
                     heroes: bool = True) -> pd.DataFrame:
    """
    Load the csv dataset file, drop the unwanted columns and scale some columns
    :param file_path: path to the csv file
    :param file_name: name of the csv file
    :param drop_cols: unwanted columns
    :param heroes: include heroes in the dataset
    :return: the preprocessed dataframe
    """
    if heroes:
        df = pd.read_csv(join(file_path, file_name), parse_dates=['start_time'], index_col=0)
    else:
        df = pd.read_csv(join(file_path, file_name), parse_dates=['start_time'], index_col=0, usecols=[*range(10)])
    df.set_index('match_id', inplace=True)

    for col in drop_cols:
        df.drop(col, axis=1, inplace=True, errors='ignore')

    float_cols = [*({'duration', 'start_time', 'region', 'radiant_gold_adv', 'radiant_xp_adv',
                     'dire_score', 'radiant_score'} - set(drop_cols))]
    df[float_cols] = (df[float_cols] - df[float_cols].min(axis=0)) / (df[float_cols].max(axis=0) -
                                                                      df[float_cols].min(axis=0))

    df['radiant_win'] = df['radiant_win'].astype(int)

    return df

