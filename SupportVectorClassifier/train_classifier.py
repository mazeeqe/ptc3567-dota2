import pandas as pd
import numpy as np
from argparse import ArgumentParser
from os.path import dirname, realpath, join
from pickle import dump
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sys import path
from typing import Tuple, Iterable, Optional, Dict, Union

# region to get and append the parent directory path
current_path = dirname(realpath(__file__))
parent_path = dirname(current_path)
path.append(parent_path)
# endregion

from utils.dataset import load_csv_dataset
from utils.utils import binary_classifier_metrics, binary_classifier_metrics_names


def split_dataset(data: pd.DataFrame, test_size: float = 0.3,
                  random_state: Optional[int] = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Separates data between inputs and outputs, and test and train data

    :param data: dataset containing all information
    :param test_size: proportion of the dataset to include in the test split
    :param random_state: controls the shuffling applied to the data before applying the split
    :return: tuple containing x train, x test, y train and y test splits
    """
    x = data.drop('radiant_win', axis=1).values
    y = data['radiant_win'].values

    return train_test_split(x, y, test_size=test_size, random_state=random_state)


def train(x_train: np.ndarray, y_train: np.ndarray, kernel: str = 'rbf', random_state: Optional[int] = None,
          save_name: Optional[str] = None) -> SVC:
    """
    Train a Support Vector Classifier

    :param x_train: input data
    :param y_train: target outputs for input train data
    :param kernel: specifies the kernel type to be used in the algorithm
    :param random_state: control the randomness of the bootstrapping of the samples used when building trees, and
                         the sampling of the features to consider when looking for the best split at each node
    :param save_name: the archive name that saves the model
    :return: classifier object
    """
    clf = SVC(kernel=kernel, random_state=random_state)
    clf.fit(x_train, y_train)

    if save_name is not None:
        with open(join(current_path, save_name), 'wb') as file:
            dump(clf, file)

    return clf


def measure_metrics(classifier: SVC, x_test: np.ndarray, y_test: np.ndarray,
                    metrics_to_use: Iterable[str] = ('confusion matrix', 'accuracy')) -> Union[Dict[str, float],
                                                                                               Dict[str, Tuple[int]]]:
    """
    Use de classifier to predict the output test from the test input and print the wanted metrics

    :param classifier: random forest classifier object
    :param x_test: test input
    :param y_test: target outputs for test input data
    :param metrics_to_use: name of the metrics wanted
    :return: a dictionary with the calculated metrics
    """
    y_pred = classifier.predict(x_test)

    metrics = binary_classifier_metrics(*confusion_matrix(y_test, y_pred).ravel())
    for metric in metrics_to_use:
        print(metrics[metric][0])

    return {m: metrics[m][1] for m in metrics}


def main(csv_data_path: str = parent_path,
         csv_data_name: str = 'Dota2MatchesData.csv',
         test_size: float = 0.3,
         random_state: int = None,
         heroes: bool = True,
         kernel: str = 'rbf',
         save_model_name: str = None,
         metrics: Iterable[str] = ('confusion matrix', 'accuracy'),
         drop_columns: Iterable[str] = ('duration', 'start_time', 'region')) -> Union[Dict[str, float],
                                                                                      Dict[str, Tuple[int]]]:
    """
    Main function that calls the other functions in order with the right parameters

    :param csv_data_path: path to the csv file
    :param csv_data_name: name of the csv file
    :param test_size: proportion of the dataset to include in the test split
    :param random_state: controls the shuffling applied to the data before applying the split
    :param heroes: true to include heroes in the dataset
    :param kernel: specifies the kernel type to be used in the algorithm
    :param save_model_name: the archive name that saves the model
    :param metrics: name of the metrics wanted to be printed
    :param drop_columns: name of the unwanted columns
    :return: a dictionary with the calculated metrics
    """
    df = load_csv_dataset(csv_data_path, csv_data_name, drop_columns, heroes)
    x_train, x_test, y_train, y_test = split_dataset(df, test_size, random_state)
    clf = train(x_train, y_train, kernel, random_state, save_model_name)
    return measure_metrics(clf, x_test, y_test, metrics)


if __name__ == '__main__':
    parser = ArgumentParser(prog='Support Vector Classifier for DOTA 2 ')
    parser.add_argument('--csv_data_path', type=str, default=parent_path,
                        help='path to the csv file')
    parser.add_argument('--csv_data_name', type=str, default='Dota2MatchesData.csv',
                        help='name of the csv file')
    parser.add_argument('--test_size', type=float, default=0.3,
                        help='proportion of the dataset to include in the test split')
    parser.add_argument('--random_state', type=int,
                        help='controls the shuffling applied to the data before applying the split')
    parser.add_argument('--heroes', type=bool, default=True,
                        help='true to include heroes in the dataset')
    parser.add_argument('--kernel', type=str, choices=('linear', 'poly', 'rbf', 'sigmoid'), default='rbf',
                        help='specifies the kernel type to be used in the algorithm')
    parser.add_argument('--save_model_name', type=str,
                        help='the archive name that saves the model if you want it to be saved')
    parser.add_argument('--metrics', type=str, nargs='*', choices=binary_classifier_metrics_names,
                        default=['confusion matrix', 'accuracy'],
                        help='name of the metrics wanted')
    parser.add_argument('--drop_columns', type=str, nargs='*',
                        choices=['duration', 'start_time', 'region', 'radiant_gold_adv', 'radiant_xp_adv',
                                 'dire_score', 'radiant_score'],
                        default=('duration', 'start_time', 'region'),
                        help='name of the unwanted columns')
    args = parser.parse_args()

    metrics = main(**vars(args))
